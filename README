quick and dirty tool that successfully assisted in brute-forcing an ssh key
encrypted with an only partially remembered passphrase.

note that the use cases for this are rare; usually when you don't remember
your passphrase and brute-forcing it is your only choice, it affects luks
passphrases or similar, as you can usually work around lost ssh keys by
physically accessing the machine. this worked because the ssh and luks
passphrases were identical, and ssh keys are so much faster to test than luks
keys.

the tool speeds up the process by about a factor of 50 (more could be easy to
achieve with pypy, and parallelization/resume is not implemented) compared to
a shell script repeatedly launching ssh-keygen.  as uses / rewrites code from
insternals of paramiko 2.0.0-1, it might not be usable with different versions
of it. (the internals rewrite is necessary because the optimization of loading
a key once as far as possible w/o a password and then running it through
different paswords is usually not needed, and the loading and decryption
normally happens in a single unresumable function).

the exrex module is another dependency; as it is not widely available (not in
debian), get it from from https://github.com/asciimoo/exrex/.

usage: ./test.py id_rsa_to_crack '((correct|horse|battery|staple) ?){8}'

(needs to be executed locally, for its internal test relies on the presence of
id_rsa_12345 which is encrypted to the passphrase "12345").

---

use, copy, modify share -- terms of the MIT license apply.
