#!/usr/bin/env python3
#!/tmp/pypy3-v5.5.0-linux64/bin/pypy3

import paramiko
from paramiko.pkey import base64, util, decodebytes, b, unhexlify, md5, Cipher, default_backend
import sys
import exrex

def _read_private_key1(self, tag, f):
    lines = f.readlines()
    start = 0
    while (start < len(lines)) and (lines[start].strip() != '-----BEGIN ' + tag + ' PRIVATE KEY-----'):
        start += 1
    if start >= len(lines):
        raise SSHException('not a valid ' + tag + ' private key file')
    # parse any headers first
    headers = {}
    start += 1
    while start < len(lines):
        l = lines[start].split(': ')
        if len(l) == 1:
            break
        headers[l[0].lower()] = l[1].strip()
        start += 1
    # find end
    end = start
    while end < len(lines) and lines[end].strip() != '-----END ' + tag + ' PRIVATE KEY-----':
        end += 1
    # if we trudged to the end of the file, just try to cope.
    try:
        data = decodebytes(b(''.join(lines[start:end])))
    except base64.binascii.Error as e:
        raise SSHException('base64 decoding error: ' + str(e))
    if 'proc-type' not in headers:
        # unencryped: done
        return data
    # encrypted keyfile: will need a password
    if headers['proc-type'] != '4,ENCRYPTED':
        raise SSHException('Unknown private key structure "%s"' % headers['proc-type'])
    try:
        encryption_type, saltstr = headers['dek-info'].split(',')
    except:
        raise SSHException("Can't parse DEK-info in private key file")
    if encryption_type not in self._CIPHER_TABLE:
        raise SSHException('Unknown private key cipher "%s"' % encryption_type)
    # if no password was passed in, raise an exception pointing out that we need one
    #if password is None:
    #    raise PasswordRequiredException('Private key file is encrypted')
    cipher = self._CIPHER_TABLE[encryption_type]['cipher']
    keysize = self._CIPHER_TABLE[encryption_type]['keysize']
    mode = self._CIPHER_TABLE[encryption_type]['mode']
    salt = unhexlify(b(saltstr))

    return (cipher, md5, salt, keysize, mode, data)

def _read_private_key2(args, password):
    cipher, md5, salt, keysize, mode, data = args

    key = util.generate_key_bytes(md5, salt, password, keysize)
    decryptor = Cipher(
        cipher(key), mode(salt), backend=default_backend()
    ).decryptor()
    return decryptor.update(data) + decryptor.finalize()

def try_(filename, regex):
    blank_key = paramiko.RSAKey.__new__(paramiko.RSAKey)
    args = _read_private_key1(blank_key, "RSA", open(filename))
    cnt = exrex.count(regex)
    print("{}: {}".format(regex, cnt))
    passwords = exrex.generate(regex)
    for i, p in enumerate(passwords):
        if i % 10000 == 0:
            print( "{} / 1 ({})           ".format(i/cnt, p), end="\r" )
        data = _read_private_key2(args, p)
        try:
            blank_key._decode_key(data)
        except paramiko.ssh_exception.SSHException:
            continue
        else:
            print("")
            return p

assert "12345" == try_("id_rsa_12345", "1234[345]")
assert None == try_("id_rsa_12345", "12345.")

print(try_(sys.argv[1], sys.argv[2]))
